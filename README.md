# Slim Framework fizzbuzz-test

1) Escribe un programa que imprima los números del 1 al 100, pero aplicando las
siguientes normas:

· Devuelve Fizz si el número es divisible por 3 o si incluye un 3 en el número.
· Devuelve Buzz si el número es divisible por 5 o si incluye un 5 en el número.
· Devuelve FizzBuzz si el número es divisible por 3 y por 5.

2) Diseñar una API REST para manejo de Stock, que incluya lista, alta y baja de stock
para un producto (no es necesaria la API para administrar productos)

* Clonar repositorio
* Correr `composer install`
* Iniciar server php `composer start`
* Correr test	`composer test`
