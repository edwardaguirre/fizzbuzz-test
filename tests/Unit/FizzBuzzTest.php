<?php

namespace Tests\Unit;


/**
 * Class FizzBuzzTest
 *
 * The Unit Test class to check FizzBuzzController
 *
 *
 * @package Controllers
 *
 * @version 1.0
 *
 */
class FizzBuzzTest extends BaseTestCase
{
    public function testToFizzBuzz() {
        $response = $this->runApp('get', '/fizzbuzz');
        $this->assertEquals(200, $response->getStatusCode());
    }
}
