<?php

namespace Controllers;

const FIZZ_DIVISOR = 3;
const BUZZ_DIVISOR = 5;

const TEXT_FIZZBUZZ = 'FizzBuzz';
const TEXT_FIZZ = 'Fizz';
const TEXT_BUZZ = 'Buzz';

const MAX_COUNT = 100;

class FizzBuzzController
{
    private function toFizzBuzz($value) {

        $isFizz = ($value % FIZZ_DIVISOR) == 0;
        $isBuzz = ($value % BUZZ_DIVISOR) == 0;

        if ($isFizz && $isBuzz) return TEXT_FIZZBUZZ;
        if ($isFizz) return TEXT_FIZZ;
        if ($isBuzz) return TEXT_BUZZ;

        return $value;

    }

    public function getNumberFizzBuzz() {
        $arrayNumber = [];
        foreach (range(1, MAX_COUNT) as $counter) {
            $arrayNumber[] = $this->toFizzBuzz($counter);
            echo $this->toFizzBuzz($counter).PHP_EOL;
        }
        if(empty($arrayNumber)) $arrayNumber = false;
        return $arrayNumber;
    }
}
