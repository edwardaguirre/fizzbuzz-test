<?php
return [
    'settings' => [
        'displayErrorDetails'       => true, // set to false in production
        'addContentLengthHeader'    => false, // Allow the web server to send the content-length header

        // Monolog settings
        'logger' => [
            'name'          => 'slim-app',
            'path'          => __DIR__ . '/../logs/app.log',
            'level'         => \Monolog\Logger::DEBUG,
        ],

        // Facebook Graph API settings
        'facebookGraphAPI' => [
            'appId'             => '125113254837130',
            'secretId'          => '0a449257a32a77070f7e66c30a2c2e9a',
            'APIversion'        => 'v2.10',
            'getUserInfo'       => [
                'userFields'    => 'id,name,about,email,first_name,gender,last_name,middle_name,link,cover,picture'
            ]
        ],
    ],
];
