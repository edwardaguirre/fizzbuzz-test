<?php
// Routes

// Product group
$app->group('/product', function () use ($app) {
    // Handles multiple HTTP request methods
    $this->map(['GET', 'POST', 'DELETE', 'PATCH', 'PUT', 'OPTIONS'], '', function ($request, $response, $args) {
        // Find, delete, patch or replace user identified by $args['id']
    })->setName('product');
    // Add a new product
    $this->post('/add', '\Controllers\ProductController:add')->setName('product-add');
    // Remove a product
    $this->delete('/remove', '\Controllers\ProductController:remove')->setName('product-remove');
    // Update a product
    $this->put('/update', '\Controllers\ProductController:update')->setName('product-update');
    // List a product
    $this->get('/list', '\Controllers\ProductController:listall')->setName('product-list');
});

$app->get('/profile/facebook/{id}', '\Controllers\FacebookGraphAPIController:getUserInfo');
$app->get('/fizzbuzz', '\Controllers\FizzBuzzController:getNumberFizzBuzz');
